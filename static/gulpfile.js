'use strict';

const gulp              = require("gulp");
const postcss           = require("gulp-postcss");
const autoprefixer      = require("gulp-autoprefixer");
const sass              = require('gulp-sass');
const browserSync       = require("browser-sync").create()
const browserReload     = (done) => {
	browserSync.reload();
	done();
}

gulp.task('sass', function(done) {
	gulp.src('./assets/_scss/*.scss')
		.pipe(sass({outputStyle: 'expanded'}))
		.pipe(autoprefixer())
		.pipe(gulp.dest('./assets/css/'));
	done();
});

gulp.task("serve", (done) => {
	const browserSyncOption = {
		server: ""
	}
	browserSync.init(browserSyncOption)
	done()
})

gulp.task("watch", (done) => {
	const browserReload = (done) => {
		browserSync.reload()
		done()
	}
	gulp.watch("./assets/**/*.scss", gulp.parallel("sass"))
	gulp.watch(["*.html","./assets/css/*.css"], browserReload)
})


gulp.task("default", gulp.series(
	"serve",
	"watch"
))