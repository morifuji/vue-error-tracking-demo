import Vue from 'vue'
import Router from 'vue-router'
import LayoutClient from '@/layout/client'
import Top from '@/pages/top'

Vue.use(Router)

export default new Router({
  linkActiveClass: 'current',
  mode: 'history',
  routes: [
    {
      path: '',
      name: 'LayoutClient',
      component: LayoutClient,
      children: [
        {
          path: 'index',
          component: Top
        },
        {
          path: '',
          redirect: '/index'
        }
      ]
    },
  ]
})
