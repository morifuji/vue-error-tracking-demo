// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from '@/App'
import router from '@/router'
import locale from 'element-ui/lib/locale/lang/ja'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import MyPlugin from '@/plugins/awesome-plugin'

import * as Sentry from '@sentry/browser'
import * as Integrations from '@sentry/integrations'

Vue.config.productionTip = false
Vue.use(ElementUI, {locale})
Vue.use(MyPlugin)

Sentry.init({
  dsn: 'https://5f6d0659695446528001763b81f35d22@sentry.io/1514164',
  integrations: [new Integrations.Vue({Vue, attachProps: true})],
})

Vue.config.errorHandler = function(err, vm, info) {
  // eslint-disable-next-line no-undef
  ga('send', 'exception', {
    'exDescription': 'catched by `Vue.config.errorHandler`\n' + err.toString(),
    'exFatal': false
  })
  // gtag('event', 'exception', {
  //   'description': 'catched by `Vue.config.errorHandler`\n' + err.toString(),
  //   'fatal': false
  // })
  console.log('catched by `Vue.config.errorHandler`', err.toString())
}

window.onerror = function(message, source, line, column, error) {
  // eslint-disable-next-line no-undef
  ga('send', 'exception', {
    'exDescription': 'catched by `window.onerror`\n' + message,
    'exFatal': false
  })
  // gtag('event', 'exception', {
  //   'description': 'catched by `window.onerror`\n' + message,
  //   'fatal': false
  // })
  console.log('catched by `window.onerror`', message)
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  renderError (h, err) {
    console.log('catched by `renderError`', err.message)
    return h('pre', { style: { color: 'red' }}, err.stack)
  }
})
