(function($) {

$(function() {

    //スムーススクロールの追加
    $('a[href*="#"]:not(.sp-menu-toggle)').smoothscroll();

    //検索用アイコンの追加
    $('input[name="ofartist_type[]"]').click( function()  {
        var checked = $('input[name="ofartist_type"]').prop('checked',true);
        var not     = $('input[name="ofartist_type"]').prop('checked',false);
        $('.cat-item label').toggleClass('selected');
        // if ($(this).is(':checked')){
        //     console.debug('チェックされていますよ (prop)');
        // }
        // else if (not){
        //     console.debug('チェックされていませんよ (prop)');

        // }
    });

    $('li input[type="submit"]').parent('li').addClass('submit-wrap');

    //フェードイン
    $(window).scroll(function (){
        $('.fadein').each(function(){
            var targetElement = $(this).offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > targetElement - windowHeight + 200){
                $(this).css('opacity','1');
                $(this).css('transform','translateY(0)');
            }
        });
    });


});

})(jQuery);
