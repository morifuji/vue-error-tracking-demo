
import dateFns from 'date-fns'
import jaLocale from 'date-fns/locale/ja'

export default {
  getRandomNumber(max, min) {
    return Math.floor(Math.random() * (max-min)) +min
  },
  createDummyListData(size = 30) {
    const datas = (new Array(size)).fill(null).map((v, i) => {
      return {
        currentPrice: this.getRandomNumber(3000, 7000),
        optimalPrice: this.getRandomNumber(3000, 7000),
        date: dateFns.format(dateFns.addDays(new Date(), this.getRandomNumber(5, 30)), 'YYYY/MM/DD(dd)', {
          locale: jaLocale
        }),
        currentPerson: this.getRandomNumber(30, 200),
        optimalPerson: this.getRandomNumber(30, 200),
        schedule: this.getRandomNumber(0, 30)
      }
    })
    console.log('LOG', datas)
    return datas
  }
}
